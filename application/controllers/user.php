<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 获取用户信息接口
 */
class User extends BASE_Controller {

	public function __construct(){
		parent::__construct();
	}

	/**
	 * 获取用户信息
	 */
	public function getInfo() {

		if ($this->_scope != 'snsapi_userinfo') {
			error_output(ERROR_INVALID_SCOPE, $this->_errno);
		}

		$userinfo = array(
			'openid' => $this->_openid,
			'nickname' => 'snowsun',
			'sex' => 1,
			'province' => 'beijing',
			'city' => 'beijing',
			'headimgurl' => 'http://tp2.sinaimg.cn/1617776085/180/5652537651/1',
		);
		output($userinfo);
	}
}
